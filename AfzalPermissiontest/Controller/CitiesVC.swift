//
//  CitiesVC.swift
//  AfzalPermissiontest
//
//  Created by Mohammed Afzal on 23/06/23.
//

import UIKit

class CitiesVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var CitiesTV: UITableView!
    
    var Country = ""
    var citiesList = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.lblTitle.text = Country
        CitiesTV.delegate = self
        
        CitiesTV.register(UINib(nibName: "ListCell", bundle: nil), forCellReuseIdentifier: "ListCell")
    }
    
    @IBAction func btnBack(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return citiesList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListCell") as! ListCell
        cell.lblName.text = citiesList[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
}
