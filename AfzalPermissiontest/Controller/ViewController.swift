//
//  ViewController.swift
//  AfzalPermissiontest
//
//  Created by Mohammed Afzal on 23/06/23.
//

import UIKit
import Photos

class ViewController: UIViewController {
    @IBOutlet weak var permissionButton: UIButton!
    
    let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.checkPermission()
        
    }
    
    
    @IBAction func btnOpenGallery(_ sender: UIButton)
    {
        
        // Access has not been determined.
        PHPhotoLibrary.requestAuthorization({ (newStatus) in
            
            if (newStatus == PHAuthorizationStatus.authorized) {
                DispatchQueue.main.async {
                    self.permissionButton.isUserInteractionEnabled = false
                    self.permissionButton.setTitle("Permission allowed Successfully", for: .normal)
                }
            }
            
            else {
                DispatchQueue.main.async {
                    self.permissionButton.isUserInteractionEnabled = false
                    self.permissionButton.setTitle("Permission denied", for: .normal)
                }
            }
        })
    }
    
    @IBAction func btnGetList(_ sender: UIButton)
    {
        let gotoCoountryVC = self.storyboard?.instantiateViewController(withIdentifier: "CountryVC") as! CountryVC
        self.navigationController?.pushViewController(gotoCoountryVC, animated: true)
    }
    
    func checkPermission()
    {
        // Get the current authorization state.
        let status = PHPhotoLibrary.authorizationStatus()
        
        if (status == PHAuthorizationStatus.authorized) {
            //            // Access has been granted.
            
            DispatchQueue.main.async {
                self.permissionButton.isUserInteractionEnabled = false
                self.permissionButton.setTitle("Permission allowed Successfully", for: .normal)
            }
        }
        
        else if (status == PHAuthorizationStatus.denied) {
            
            DispatchQueue.main.async {
                self.permissionButton.isUserInteractionEnabled = false
                self.permissionButton.setTitle("Permission denied", for: .normal)
            }
            
            let alert = UIAlertController(title: "Permission Required", message: "Please allow Photos permission from setting", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "Open Setting", style: .cancel, handler: { (alert) -> Void in
                UIApplication.shared.open(self.settingsAppURL, options: [:], completionHandler: nil)
            }))
            
            self.present(alert, animated: true)
        }
        
        else if (status == PHAuthorizationStatus.notDetermined) {
            
        }
        
        else if (status == PHAuthorizationStatus.restricted) {
            // Restricted access - normally won't happen.
        }
    }
}

