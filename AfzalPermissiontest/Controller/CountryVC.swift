//
//  CountryVC.swift
//  AfzalPermissiontest
//
//  Created by Mohammed Afzal on 23/06/23.
//

import UIKit
import Alamofire
import KRProgressHUD

class CountryVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var CouontryTV: UITableView!
    var countryList = [CountryList]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CouontryTV.delegate = self
        CouontryTV.register(UINib(nibName: "ListCell", bundle: nil), forCellReuseIdentifier: "ListCell")
        
        self.getCountryList()
    }
    
    func getCountryList()
    {
        KRProgressHUD.show()
        AF.request(baseURL, method: .get, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
                
            case .failure(let error):
                print(error.localizedDescription)
                KRProgressHUD.dismiss()
                
            case .success(_):
                
                do{
                    let result = try JSONDecoder().decode(CountryModel.self , from: response.data!)
                    self.countryList = result.data
                    DispatchQueue.main.async {
                        self.CouontryTV.reloadData()
                        KRProgressHUD.dismiss()
                    }
                }
                catch{
                    print(error.localizedDescription)
                    KRProgressHUD.dismiss()
                }
            }
        }
        
    }
    
    @IBAction func btnBack(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.countryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListCell") as! ListCell
        let data = self.countryList[indexPath.row]
        cell.lblName.text = data.country ?? ""
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let gotoCities = self.storyboard?.instantiateViewController(withIdentifier: "CitiesVC") as! CitiesVC
        gotoCities.citiesList = self.countryList[indexPath.row].cities
        gotoCities.Country = self.countryList[indexPath.row].country ?? "Cities"
        self.navigationController?.pushViewController(gotoCities, animated: true)
    }
}
