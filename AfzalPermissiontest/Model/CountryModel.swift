//
//  CountryModel.swift
//  AfzalPermissiontest
//
//  Created by Mohammed Afzal on 23/06/23.
//

import Foundation

struct CountryModel: Codable
{
    var error: Bool?
    var msg : String?
    var data : [CountryList]
}

struct CountryList: Codable
{
    var iso2: String?
    var iso3: String?
    var country: String?
    var cities : [String]
}

